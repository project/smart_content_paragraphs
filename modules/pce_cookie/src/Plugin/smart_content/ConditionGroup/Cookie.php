<?php

namespace Drupal\pce_cookie\Plugin\smart_content\ConditionGroup;

use Drupal\smart_content\ConditionGroup\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "cookie",
 *   label = @Translation("Cookie"),
 *   weight = 0,
 * )
 */
class Cookie extends ConditionGroupBase {

}
