<?php

namespace Drupal\pce_geobrowser\Plugin\smart_content\ConditionGroup;

use Drupal\smart_content\ConditionGroup\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "geobrowser",
 *   label = @Translation("Geolocation - Browser"),
 *   weight = 0,
 * )
 */
class Geobrowser extends ConditionGroupBase {

}
