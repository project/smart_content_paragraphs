<?php

namespace Drupal\pce_geocloudflare\Plugin\smart_content\ConditionGroup;

use Drupal\smart_content\ConditionGroup\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "geocloudflare",
 *   label = @Translation("Geolocation - Cloudflare"),
 *   weight = 0,
 * )
 */
class Geocloudflare extends ConditionGroupBase {

}
