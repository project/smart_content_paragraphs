<?php

namespace Drupal\pce_node\Plugin\smart_content\ConditionGroup;

use Drupal\smart_content\ConditionGroup\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "node",
 *   label = @Translation("Node"),
 *   weight = 0,
 * )
 */
class Node extends ConditionGroupBase {

}
