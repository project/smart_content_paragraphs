<?php

namespace Drupal\pce_device\Plugin\smart_content\ConditionGroup;

use Drupal\smart_content\ConditionGroup\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "device",
 *   label = @Translation("Device & Network"),
 *   weight = 0,
 * )
 */
class Device extends ConditionGroupBase {

}
